#ifndef NETWORK_H
#define NETWORK_H

#include <string>
#include <vector>
#include <mutex>

using namespace std;

namespace MusicStreamingNetwork
{
    // Tether our packetType to int size, to make sure we get the same size no matter if client/server was compiled
    // with a compiler that turn the enum into something smaller than an int...
    enum class packetType : int
    {
        PACKET_REQUEST_SONG,
        PACKET_COVER_ART,
        PACKET_COVER_ART_END,
        PACKET_MUSIC_STREAM_SIZE,
        PACKET_MUSIC_STREAM,
        PACKET_END_CONNECTION
    };

    struct Packet
    {
        packetType      m_packetType;
        unsigned int    m_dataSize;
        char*           m_data;
    };

    struct NetworkTraffic
    {
	    std::vector<Packet> m_recv;
	    std::vector<Packet> m_send;

        // Mutexs are used to make it thread safe operating on the vectors of receive/send packets
	    std::mutex          m_recvMutex;
	    std::mutex 		    m_sendMutex;
    };

    constexpr int s_dataSendSize = 256; // Total packet size may never exceed 256 bytes, if so the packet will not be sent.
    constexpr int s_packetMaxDataSize = s_dataSendSize - sizeof(Packet) + sizeof(char*);

    void    FreePacketData(Packet &packet);

    int     ClientConnectToServer(const string &machineName);

    int     CreateSocketAndListen();
    int     ServerWaitForClientToConnect(int theSocket);

    void    CreateRequestSongPacket(Packet &pack, const string &songName);

    bool    ReceivePacket(const int socket, Packet &packet);
    bool    SendPacket(const int socket, const Packet &packet);
    void    ShutDownConnection(const int socket);
}

#endif