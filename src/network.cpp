#include <sys/types.h> //contains a number of basic derived types that should be used whenever appropriate
#include <arpa/inet.h> // defines in_addr structure
#include <sys/socket.h> // for socket creation
#include <netinet/in.h> //contains constants and structures needed for internet domain addresses
#include <unistd.h>
#include <poll.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include "network.h"

namespace MusicStreamingNetwork
{
    void FreePacketData(Packet &packet)
    {
        if(packet.m_dataSize > 0)
        {
            delete[] packet.m_data;
        }
    }
    //----------------------------------------------------------------------
    // We could just push the struct straight out to the socket and then read it back on the client,
    // but we would then bind our server/client to have the same memory-layout and endianess etc which
    // is quite unappealing. Another approach is to convert packets to xml - but this can be very performance intense
    // to parse/read. For more complex data structure serializing a library such as google protobuf might be better.
    // We will go for a approach were we hard-code how packets are serialized and read back. Since our protocol
    // is small and we want to understand all parts of what our client/server solution does we opt for this solution.
    bool SendPacket(const int socket, const Packet &packet)
    {        
        char dataSend[s_dataSendSize];

        struct pollfd fds[1];
        memset(fds, 0, sizeof(fds));

        fds[0].fd = socket;
        fds[0].events = POLLOUT;

        int ready = poll(fds, 1, 50); // Check if send would cause a block
        if(ready != -1 && (fds[0].revents & POLLOUT)) // Write on this socket will not cause a block
        {
            // Put thePacket into dataSend and send it
            size_t offset = 0;
            memcpy(dataSend + offset, &packet.m_packetType, sizeof(packet.m_packetType));
            offset += sizeof(packet.m_packetType);
            memcpy(dataSend + offset, &packet.m_dataSize, sizeof(packet.m_dataSize));
            offset += sizeof(packet.m_dataSize);
            if(packet.m_dataSize > 0) // Not all packets have a data load, for example cover art end packet.
            {
                memcpy(dataSend + offset, packet.m_data, MusicStreamingNetwork::s_packetMaxDataSize);
            }
            
            // Send it
            int sendRetV = send(socket, dataSend, MusicStreamingNetwork::s_dataSendSize, 0);           
            if(sendRetV == -1)
            {
                cout << "send error, errno:" << errno << endl;
            }

            return true;

        }
        return false;
    }
    //----------------------------------------------------------------------
    void ShutDownConnection(const int socket)
    {
        close(socket);
    }
    //----------------------------------------------------------------------
    bool ReceivePacket(const int socket, Packet &thePacket)
    {
        struct pollfd fds[1];
        memset(fds, 0, sizeof(fds));

        fds[0].fd = socket;
        fds[0].events = POLLIN;

        int ready = poll(fds, 1, 50); // Check if read would cause a block
        if(ready != -1 && (fds[0].revents & POLLIN)) // Read on this socket will not cause a block
        {
            unsigned int totalPacketSize = MusicStreamingNetwork::s_dataSendSize;
            char dataReceive[s_dataSendSize];
        
            // Read back the packet
            int sizeRead = read(socket, dataReceive, totalPacketSize);
            if(sizeRead > 0)
            {
                // There is no guarantee that we will get all the data in one read call, so we might need to make multiple calls to make sure
                // we get it all. If you run on your local machine you may never see this code "in action", but in a real world
                // scenario with a server on another machine it will happen frequently.
                int curPos = sizeRead;
                while(sizeRead < totalPacketSize)
                {
                    sizeRead += read(socket, dataReceive + curPos, totalPacketSize - curPos);
                    curPos = sizeRead;

                    if(sizeRead == 0)
                    {
                        break;
                    }
                }

                if(sizeRead == totalPacketSize)
                {                    
                    // Transform the data we received into a packet struct
                    size_t offset = 0;
                    memcpy(&thePacket.m_packetType, dataReceive + offset, sizeof(thePacket.m_packetType));
                    offset += sizeof(thePacket.m_packetType);
                    memcpy(&thePacket.m_dataSize, dataReceive + offset, sizeof(thePacket.m_dataSize));
                    offset += sizeof(thePacket.m_dataSize);

                    if(thePacket.m_dataSize > 0) // Not all packets have a data load, for example PACKET_COVER_ART_END
                    {
                        thePacket.m_data = new char[thePacket.m_dataSize];
                        memcpy(thePacket.m_data, dataReceive + offset, thePacket.m_dataSize);
                    }

                    return true;
                }
                else // Something went wrong :(
                {
                    cout << "Unexpected packet size: " << totalPacketSize << ", sizeRead: " << sizeRead << " Packet dropped." << endl;
                }
            }            
        }

        return false;
    }
    //----------------------------------------------------------------------
    int ClientConnectToServer(const string &machineName)
    {
        int socketCreated = 0;
        struct sockaddr_in ipOfServer;
        socketCreated = socket(AF_INET, SOCK_STREAM, 0); // Create a blocking socket
        memset(&ipOfServer, '0', sizeof(ipOfServer));
        ipOfServer.sin_family = AF_INET;

        // Convert IPv4 and IPv6 addresses from text to binary form
        if(inet_pton(AF_INET, machineName.c_str(), &ipOfServer.sin_addr)<=0)
        {
            cout << "Invalid address/ Address not supported" << endl;
            return -1;
        }	

        ipOfServer.sin_port = htons(3017); // this is the port number of running server

        // Wait until we are connected to the server
        int connectRes = connect(socketCreated, (struct sockaddr *)&ipOfServer, sizeof(ipOfServer));
        if(connectRes < 0 )
        {
            cout << "Connection failed" << endl;
            return -1;
        }

        return socketCreated;
    }
    //----------------------------------------------------------------------
    int CreateSocketAndListen()
    {
        int theSocket = 0;
        struct sockaddr_in ipOfServer;
        theSocket = socket(AF_INET, SOCK_STREAM, 0); // creating a blocking socket

        memset(&ipOfServer, '0', sizeof(ipOfServer));
        ipOfServer.sin_family = AF_INET;
        ipOfServer.sin_addr.s_addr = htonl(INADDR_ANY);
        ipOfServer.sin_port = htons(3017); // this is the port number of running server
        bind(theSocket, (struct sockaddr*)&ipOfServer , sizeof(ipOfServer));
        listen(theSocket , 1000); // Start listening (maximum 1000 clients)

        return theSocket;
    }
    //----------------------------------------------------------------------
    int ServerWaitForClientToConnect(int theSocket)
    {
        int socketForClient = 0;
        socketForClient = accept(theSocket, (struct sockaddr*)NULL, NULL);
        return socketForClient;
    }
    //----------------------------------------------------------------------
    void CreateRequestSongPacket(Packet &pack, const string &songName)
    {
        pack.m_packetType = packetType::PACKET_REQUEST_SONG;
        pack.m_dataSize = songName.size() + 1; // We want the null-terminating character as well!
        pack.m_data = new char[MusicStreamingNetwork::s_dataSendSize];
        memcpy(pack.m_data, songName.c_str(), songName.size() + 1);
    }
    //----------------------------------------------------------------------
}    