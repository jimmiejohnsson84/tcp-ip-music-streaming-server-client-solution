package main

import ("fmt"
        "io/ioutil"
        "net"
        "encoding/binary"
)

const (
    PACKET_REQUEST_SONG = iota
    PACKET_COVER_ART
    PACKET_COVER_ART_END
    PACKET_MUSIC_STREAM_SIZE
    PACKET_MUSIC_STREAM
    PACKET_END_CONNECTION
)

type Packet struct {
    m_packetType uint32
    m_dataSize uint32
    m_data [256 - (4 * 2)]byte
}
//---------------------------------------------------------------------------
func ReadPacket(conn net.Conn, packet *Packet) bool {

    buf := make([]byte, 256)
    packetRead := false
    // Read the incoming connection into the buffer    
    for
    {
        packetLen, err := conn.Read(buf)
        if err != nil {
            fmt.Println("Could'nt read packet")   
            break;
        }

        if packetLen == 256 {
            packetRead = true
            break; // All data from packet recevied
        }
    }

    if packetRead {
        // Read it back to the Packet data structure
        packet.m_packetType = binary.LittleEndian.Uint32(buf[0:4])
        packet.m_dataSize = binary.LittleEndian.Uint32(buf[4:8])
        if packet.m_dataSize != 0 {
            copy(packet.m_data[0:256 - (4 * 2)], buf[8: 256])
        }
    }

    return packetRead
}
//---------------------------------------------------------------------------
func SendPacket(conn net.Conn, packet *Packet) {

    // Transfer the content of the Packet to buf and transfer it over connection
    buf := make([]byte, 256)      
    binary.LittleEndian.PutUint32(buf[0:], packet.m_packetType) // Packet type
    binary.LittleEndian.PutUint32(buf[4:], packet.m_dataSize) // Data size
    copy(buf[8:256], packet.m_data[0 : packet.m_dataSize]) // Data load

    conn.Write(buf);
}
//---------------------------------------------------------------------------
func SendCoverArt(conn net.Conn, songRequested string) {
      
    // Open the song cover art request
    var coverArtFileName string
    coverArtFileName = "sfx/coverArt_" + songRequested
    fmt.Println("Opening cover art: ", coverArtFileName)

    coverArt, err := ioutil.ReadFile(coverArtFileName)
    if err != nil {
        fmt.Println("Could'nt open cover art, error: ", err.Error())
    }

    var coverArtLen uint32 = uint32(len(coverArt))
    var currentCoverArtPos uint32 = 0
    var dataLoadMaxSize uint32 = 256 - (4 * 2)

    for currentCoverArtPos < coverArtLen {
        var dataSize uint32 = dataLoadMaxSize

        if currentCoverArtPos + dataLoadMaxSize > coverArtLen {
            dataSize = coverArtLen - currentCoverArtPos
        }

        var coverArtPack Packet
        coverArtPack.m_packetType = PACKET_COVER_ART;
        coverArtPack.m_dataSize = dataSize
        copy(coverArtPack.m_data[0 : coverArtPack.m_dataSize], coverArt[currentCoverArtPos : currentCoverArtPos + dataSize])
        SendPacket(conn, &coverArtPack)
        currentCoverArtPos += dataSize
    }

    var coverArtEndPack Packet;
    coverArtEndPack.m_packetType = PACKET_COVER_ART_END
    coverArtEndPack.m_dataSize = 0
    SendPacket(conn, &coverArtEndPack)
}
//---------------------------------------------------------------------------
func SendMusic(conn net.Conn, songRequested string) {

    musicFileName := "sfx/" + songRequested
    musicContent, err := ioutil.ReadFile(musicFileName)
    if err != nil {
        fmt.Println("Could'nt read music file")
    }

    var musicFileLen uint32 = uint32(len(musicContent))

    var musicSizePack Packet;
    musicSizePack.m_packetType = PACKET_MUSIC_STREAM_SIZE;
    musicSizePack.m_dataSize = 4;
    binary.LittleEndian.PutUint32(musicSizePack.m_data[0:], musicFileLen);
    SendPacket(conn, &musicSizePack);

    fmt.Println("Sending music...")

    var dataLoadMaxSize uint32 = 256 - (4 * 2)
    var currentMusicPos uint32 = 0
    for currentMusicPos < musicFileLen {
        var dataSize uint32 = dataLoadMaxSize

        if currentMusicPos + dataLoadMaxSize > musicFileLen {
            dataSize = musicFileLen - currentMusicPos
        }

        var musicPack Packet;
        musicPack.m_packetType = PACKET_MUSIC_STREAM;
        musicPack.m_dataSize = dataSize;
        copy(musicPack.m_data[0 : musicPack.m_dataSize], musicContent[currentMusicPos : currentMusicPos + dataSize]);
        SendPacket(conn, &musicPack);
        fmt.Print("*")
        currentMusicPos += dataSize
    }

    fmt.Println("")
    fmt.Println("All music has been sent!")
}    


func main() {
    fmt.Println("Waiting for connection")
    l, err := net.Listen("tcp", ":3017")
    if err != nil {
        fmt.Println("Network error")
    }
    
    defer l.Close() // Make sure Close gets called before we exit main

    c, err := l.Accept()
    if err != nil {
        fmt.Println("Network error")
    }



    fmt.Println("Client connected: ", c.RemoteAddr().String())

    // Main loop
    clientDisconnect := false
    for {

        // Client sent a disconnect packet
        if clientDisconnect {
            break;
        }

        // Wait for a packet from the client
        var clientPacket Packet
        if ReadPacket(c, &clientPacket) {
            switch clientPacket.m_packetType {

                case PACKET_REQUEST_SONG:
                    songRequested := string(clientPacket.m_data[0:clientPacket.m_dataSize - 1]) // Drop terminating '\n', hence the m_dataSize - 1
                    fmt.Println("Song req: ", songRequested)

                    SendCoverArt(c, songRequested)
                    SendMusic(c, songRequested)
                // Go language does'nt use break cases between switch case statement...
                case PACKET_END_CONNECTION:
                    fmt.Println("Disconnect packet recevied")
                    clientDisconnect = true

            }
        }
    }
}