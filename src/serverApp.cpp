#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>

#include <unistd.h> // fork
#include <sys/wait.h>

#include "serverApp.h"
#include "network.h"

void ServerApplication::StartServer()
{
    m_socket = MusicStreamingNetwork::CreateSocketAndListen();
}
//----------------------------------------------------------------------
int ServerApplication::WaitForClientToConnect()
{
    return MusicStreamingNetwork::ServerWaitForClientToConnect(m_socket);    
}
//----------------------------------------------------------------------
void ServerApplication::ServerMainLoop()
{
    while(true)
    {
        std::cout << "[PARENT] Waiting for clients to join on port 3017..." << std::endl;
        int childProcessSocket = WaitForClientToConnect(); // Blocking call 

        // Check on our exisiting child process, if any of them have terminated close their sockets.        
        for(auto &childProcessData : m_childProcesses)    
        {
            int status;
            if(waitpid(childProcessData.m_pid, &status, WNOHANG) != 0) // The child process has changed state, ie it has terminated.
            {
                std::cout << "[PARENT] Closing  child socket " << childProcessData.m_socket << std::endl;
                close(childProcessData.m_socket); // We need to close it for the parent as well
                childProcessData.m_hasExited = true;
            }
        }
        auto terminatedProcess = std::ranges::remove_if(m_childProcesses, [](const ChildProcessData &c) {return c.m_hasExited;});
        m_childProcesses.erase(terminatedProcess.begin(), terminatedProcess.end());

        std::cout << "[PARENT] New client joined, spawning child process" << std::endl;

        int processId = fork();
        if (processId < 0)
        {
            std::cout << "[PARENT] fork failed" << std::endl;
            break; // Give up
        }

        if(processId != 0) // Parent process
        {
            m_childProcesses.push_back({childProcessSocket, processId, false}); // Register child process
        }
        else // Child process
        {
            std::cout << "[CHILD] Child process active" << std::endl;
        
            bool clientDisconnected = false;
            while(!clientDisconnected) // Check for song requests until client sends a disconnect packet
            {
                MusicStreamingNetwork::Packet clientPacket;
                if(MusicStreamingNetwork::ReceivePacket(childProcessSocket, clientPacket))
                {
                    switch(clientPacket.m_packetType)
                    {
                        case MusicStreamingNetwork::packetType::PACKET_END_CONNECTION:
                        {
                            clientDisconnected = true;
                            std::cout << "[CHILD] Client sent end of sessions package" << std::endl;
                        }
                        break;

                        case MusicStreamingNetwork::packetType::PACKET_REQUEST_SONG:
                        {
                            std::cout << "[CHILD] Song request recevied" << std::endl;
                            // Send cover art
                            string songRequested(clientPacket.m_data);
                            MusicStreamingNetwork::FreePacketData(clientPacket);

                            SendCoverArt(childProcessSocket, songRequested);            
                            cout << "[CHILD] Cover art sent" << endl;

                            SendMusic(childProcessSocket, songRequested);
                        }
                        break;
                    }
                }
            }
            close(childProcessSocket); // Close socket for the child
            break; // Exit child process out of ServerMainLoop
        }
    }
}
//----------------------------------------------------------------------
void ServerApplication::SendCoverArt(int socket, const std::string &songToPlay)
{
    std::string coverArtResName = "sfx/coverArt_" + songToPlay;

    std::cout << "[CHILD] opening " << coverArtResName << std::endl;
    std::ifstream coverArtFile(coverArtResName.c_str(), ios::out | ios::binary);

    // get length of file
    coverArtFile.seekg (0, coverArtFile.end);
    int coverArtFileSize = coverArtFile.tellg();
    coverArtFile.seekg (0, coverArtFile.beg);

    int coverArtDataSent = 0;
    char dataLoad[MusicStreamingNetwork::s_packetMaxDataSize];
    
    while(coverArtDataSent < coverArtFileSize)
    {
        int nrBytesRead = coverArtFile.readsome(dataLoad, MusicStreamingNetwork::s_packetMaxDataSize);

        MusicStreamingNetwork::Packet coverArt;
        coverArt.m_packetType = MusicStreamingNetwork::packetType::PACKET_COVER_ART;
        coverArt.m_dataSize = nrBytesRead;
        coverArt.m_data = dataLoad; // Avoid allocating memory all the time here...

        bool packetSent = false;
        while(!packetSent)
        {
            packetSent = MusicStreamingNetwork::SendPacket(socket, coverArt);
        }
        
        coverArtDataSent += nrBytesRead;
    }
    coverArtFile.close();

    // Send 
    MusicStreamingNetwork::Packet coverArtEnd;
    coverArtEnd.m_packetType = MusicStreamingNetwork::packetType::PACKET_COVER_ART_END;
    coverArtEnd.m_dataSize = 0;

    MusicStreamingNetwork::SendPacket(socket, coverArtEnd);
    std::cout << "[CHILD] coverArt file sent" << std::endl;
}
//----------------------------------------------------------------------
void ServerApplication::SendMusic(int socket, const std::string &songToPlay)
{
    std::string musicResName = "sfx/" + songToPlay;
    std::ifstream musicFile(musicResName.c_str(), ios::out | ios::binary);
    // get length of file
    musicFile.seekg (0, musicFile.end);
    int musicFileSize = musicFile.tellg();
    musicFile.seekg (0, musicFile.beg);

    std::cout << "[CHILD] Sending music..." << std::endl;

    MusicStreamingNetwork::Packet musicStreamSize;
    musicStreamSize.m_packetType = MusicStreamingNetwork::packetType::PACKET_MUSIC_STREAM_SIZE;
    musicStreamSize.m_dataSize = sizeof(int);
    musicStreamSize.m_data = new char[MusicStreamingNetwork::s_packetMaxDataSize];
    memcpy(musicStreamSize.m_data, &musicFileSize, sizeof(int));

    bool packetSent = false;
    while(!packetSent)
    {
        packetSent = SendPacket(socket, musicStreamSize);   
    }

    cout << "[CHILD] music size packet sent" << endl;

    int musicDataSent = 0;
    char dataLoad[MusicStreamingNetwork::s_packetMaxDataSize];

    unsigned int nrMusicStreamPacksSent = 0;
    while(musicDataSent < musicFileSize)
    {
        int nrBytesRead = musicFile.readsome(dataLoad, MusicStreamingNetwork::s_packetMaxDataSize); 

        MusicStreamingNetwork::Packet musicStream;
        musicStream.m_packetType = MusicStreamingNetwork::packetType::PACKET_MUSIC_STREAM;
        musicStream.m_dataSize = nrBytesRead;
        musicStream.m_data = dataLoad;

        bool packetSent = false;
        while(!packetSent)
        {
            packetSent = MusicStreamingNetwork::SendPacket(socket, musicStream);
        }

        musicDataSent += nrBytesRead;

        if(nrBytesRead <= 0)
        {
            std::cout << "[CHILD] error reading: " << nrBytesRead << std::endl;
        }
        nrMusicStreamPacksSent++;
    }

    musicFile.close();
    std::cout << "[CHILD] Sent " << musicDataSent << "Amount of data in " << nrMusicStreamPacksSent << "nr of packets, whole file sent" << std::endl;
}
