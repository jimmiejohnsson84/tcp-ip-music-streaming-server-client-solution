// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <signal.h>

#include "network.h"
#include "serverApp.h"
 
int main(int argc, char** argv) 
{
    //using namespace std;
    //using namespace MusicStreamingNetwork;

	// No zombies thanks
	signal(SIGCHLD, SIG_IGN);

	ServerApplication serverApp;
    serverApp.StartServer();

    serverApp.ServerMainLoop();

	return 0;
}

