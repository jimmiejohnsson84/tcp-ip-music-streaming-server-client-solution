#ifndef SERVER_APP_H
#define SERVER_APP_H

#include <string>
#include <vector>

class ServerApplication
{
    public:

    void    StartServer();
    void    ServerMainLoop();
   
    private:

    int     WaitForClientToConnect();
    void    SendCoverArt(int socket, const std::string &songToPlay);
    void    SendMusic(int socket, const std::string &songToPlay);


    int m_socket;

    struct ChildProcessData
    {
        int     m_socket;
        int     m_pid;
        bool    m_hasExited;
    };

    std::vector<ChildProcessData> m_childProcesses;
};

#endif