#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <thread>
#include <cstring>
#include <atomic>

#include "network.h"
#include "audio.h"

using namespace std;
using namespace MusicStreamingNetwork;

class ClientSessionData
{
	public:
	ClientSessionData() { m_songStarted = false; m_printMainMenu = false;}

	std::string		m_serverName;
	std::string		m_coverArt;
	bool			m_songStarted;
	bool			m_printMainMenu;

	StreamingMusic	m_streamingMusic;

	NetworkTraffic	m_networkTraffic;
};


// Client only talks to one server, so all we need to do is handle incoming packets and outgoing packets in threads.
// If we receive a packet, we grab a lock and put it into the receive queue.
void HandlePackets(int socket, std::atomic<bool> &clientRunning, NetworkTraffic &networkTraffic)
{
	while(clientRunning)
	{		
		// Receive packets
		Packet recivePacket;		
		if(ReceivePacket(socket, recivePacket))
		{
			std::lock_guard<std::mutex> guard_read(networkTraffic.m_recvMutex);
			networkTraffic.m_recv.push_back(recivePacket);
		}

		// Send packets
		std::lock_guard<std::mutex> guard_read(networkTraffic.m_sendMutex);		
		if(!networkTraffic.m_send.empty())
		{								
			for(auto packet : networkTraffic.m_send)
			{
				bool packetSent = false;
				while(!packetSent)
				{
					packetSent = SendPacket(socket, packet);
				}
				FreePacketData(packet);
			}
			networkTraffic.m_send.clear();
		}
	}
}
//----------------------------------------------------------------------
void PutPacketOnSendQueue(const Packet &packet, NetworkTraffic &networkTraffic)
{
	std::lock_guard<std::mutex> guard(networkTraffic.m_sendMutex);
	networkTraffic.m_send.push_back(packet);
}
//----------------------------------------------------------------------
void HandleCoverArtPacket(const Packet &coverArtPacket, ClientSessionData &clientSessionData)
{
	// This is needed, std string cant handle none null-terminated string so we cant use something like append or the += operator!
	for(int i = 0; i < coverArtPacket.m_dataSize; i++)
	{
		clientSessionData.m_coverArt.push_back(coverArtPacket.m_data[i]);
	}
}
//----------------------------------------------------------------------
void HandleCoverArtEndPacket(ClientSessionData &clientSessionData)
{
	cout << clientSessionData.m_coverArt << endl;	
}
//----------------------------------------------------------------------
void HandleMusicStreamPacket(ClientSessionData &clientSessionData, const Packet &musicStreamPacket)
{
	clientSessionData.m_streamingMusic.FetchAudioToMemory(musicStreamPacket.m_data, musicStreamPacket.m_dataSize);
}
//----------------------------------------------------------------------
void HandleMusicStreamSizePacket(ClientSessionData &clientSessionData, const Packet &musicStreamSizePacket)
{
	int musicFileSize = 0;
	memcpy(&musicFileSize, musicStreamSizePacket.m_data, musicStreamSizePacket.m_dataSize);
	clientSessionData.m_streamingMusic.StartFetchAudio(musicFileSize);
}
//----------------------------------------------------------------------
void ProcessPacketsReceived(std::atomic<bool> &clientRunning, ClientSessionData &clientSessionData)
{
	while(clientRunning)
	{
		std::lock_guard<std::mutex> guard(clientSessionData.m_networkTraffic.m_recvMutex);
		while(!clientSessionData.m_networkTraffic.m_recv.empty())
		{
			Packet &thePacket = clientSessionData.m_networkTraffic.m_recv.front();
			if(thePacket.m_packetType == packetType::PACKET_COVER_ART)
			{
				HandleCoverArtPacket(thePacket, clientSessionData);
				FreePacketData(thePacket);
				clientSessionData.m_networkTraffic.m_recv.erase(clientSessionData.m_networkTraffic.m_recv.begin());
			}	
			else if(thePacket.m_packetType == packetType::PACKET_COVER_ART_END)
			{
				HandleCoverArtEndPacket(clientSessionData);
				FreePacketData(thePacket);
				clientSessionData.m_networkTraffic.m_recv.erase(clientSessionData.m_networkTraffic.m_recv.begin());
			}
			else if(thePacket.m_packetType == packetType::PACKET_MUSIC_STREAM_SIZE)
			{
				HandleMusicStreamSizePacket(clientSessionData, thePacket);
				FreePacketData(thePacket);
				clientSessionData.m_networkTraffic.m_recv.erase(clientSessionData.m_networkTraffic.m_recv.begin());
			}
			else if(thePacket.m_packetType == packetType::PACKET_MUSIC_STREAM)
			{
				HandleMusicStreamPacket(clientSessionData, thePacket);
				FreePacketData(thePacket);
				clientSessionData.m_networkTraffic.m_recv.erase(clientSessionData.m_networkTraffic.m_recv.begin());

				if(clientSessionData.m_streamingMusic.ReadyToPlayMusic() && !clientSessionData.m_songStarted)
				{
					clientSessionData.m_streamingMusic.StartMusic();
					clientSessionData.m_songStarted = true;
					clientSessionData.m_printMainMenu = true;					
				}
			}
		}
	}
}
//----------------------------------------------------------------------
void PrintMainMenu()
{
	cout << "***********" << endl;
	cout << "S: Request a new song" << endl;
	cout << "Q: Quit" << endl;
	cout << "***********" << endl;
}


int main(int argc, char** argv) 
{
	std::atomic<bool> clientRunning  = { true } ;

	ClientSessionData clientSessionData;

	InitAudio();

	cout << "Please specify machine to connect to: " ;
	cin >> clientSessionData.m_serverName;

	cout << "Connecting to server..." << endl;

	int socketForClient = ClientConnectToServer(clientSessionData.m_serverName);

	string songToPlay;
	cout << "Specify which song you would like to hear: ";
	cin >> songToPlay;
	clientSessionData.m_streamingMusic.SetSongName(songToPlay.c_str());

	Packet requestSong;
	CreateRequestSongPacket(requestSong, songToPlay);
	PutPacketOnSendQueue(requestSong, clientSessionData.m_networkTraffic);

	// Connection established, start our receive/send threads
	std::thread threadHandlePackets(HandlePackets, socketForClient, std::ref(clientRunning), std::ref(clientSessionData.m_networkTraffic));

	// Start handling packets receveid
	std::thread threadProcessPackets(ProcessPacketsReceived, std::ref(clientRunning), std::ref(clientSessionData));

	bool userExited = false;
	char input;
	while(!userExited)
	{
		if(clientSessionData.m_printMainMenu)
		{
			clientSessionData.m_printMainMenu = false;
			PrintMainMenu();
			cin >> input;
			if(input == 'S')
			{
				clientSessionData.m_streamingMusic.StopMusic();
				clientSessionData.m_songStarted = false;

				cout << "Specify which song would you like to hear: ";
				cin >> songToPlay;
				clientSessionData.m_streamingMusic.SetSongName(songToPlay.c_str());			
				clientSessionData.m_coverArt.clear();

				Packet requestSong;
				CreateRequestSongPacket(requestSong, songToPlay);
				PutPacketOnSendQueue(requestSong, clientSessionData.m_networkTraffic);			
			}
			if(input == 'Q')
			{
				userExited = true;
			}
		}
	}

	// Inform server that we have disconnected
	Packet disconnect;
	disconnect.m_dataSize = 0;
	disconnect.m_packetType = MusicStreamingNetwork::packetType::PACKET_END_CONNECTION;
	PutPacketOnSendQueue(disconnect, clientSessionData.m_networkTraffic);

	bool disconnectPacketSent = false;
	while(!disconnectPacketSent)
	{
		std::lock_guard<std::mutex> guard(clientSessionData.m_networkTraffic.m_sendMutex);
		if(clientSessionData.m_networkTraffic.m_send.empty())
		{
			disconnectPacketSent = true;
			cout << "Informing server that we want to end session" << endl;
		}
	}

	// Stop 'threadHandlePackets' and 'threadProcessPackets' threads.
	clientRunning = false;

	threadProcessPackets.join();
	threadHandlePackets.join();

	KillAudio();

	ShutDownConnection(socketForClient);

	return 0;
}


