TCP/IP Music streaming server and client
========================

Youtube video:
========================

Here is a Youtube video describing the architecture of the server and client as well as a walkthrough of the code.
The project is meant to be educational and to make it easier to follow along some of the code is laid out pretty flat.
My hope is that this makes it easier for you to organize the code to whatever architecture you prefer.
[![C++ TCP/IP Music Streaming Video](https://img.youtube.com/vi/VrOZ9BiWd-8/0.jpg)](https://www.youtube.com/watch?v=VrOZ9BiWd-8)


Platforms:
========================

This project is made for Linux. With a bit of elbow grease you could probably get it to run on Windows as well.


How to build:
========================

```bash
To build server:
make dir build
step into build
cmake ..
make musicServer

To build client:
Same steps as for server then run:
make musicClient
```

How to run:
========================

```bash
To run server:
run musicServer
The server will wait for clients to connect and then handle incoming song requests.

To run client:
run musicClient
The music client will ask for an ip adress to connect to. After that, an attempt to connect is made. If connection is established, you
can request for a song to be played. Music will then be streamed from the server to client. Once a song is playing, you can stop it and request
a new song or to quit the client.
```


Libraries used:
========================

This project uses the audio library [Irrklang](https://www.ambiera.com/irrklang/index.html). This library is included in the repository so no need to fetch and install it on your own.


Go language server
========================

This is still a work in progress!

There is a version of the music server available in the src/golangServer directory. At the time of writing, the server can only serve one single client.
A future version will be added that can handle multiple clients just like the C++ version.


```bash
To build go language server:
step into the src/golangServer directory
go build musicServer

To run the go language server:
run musicServer
```